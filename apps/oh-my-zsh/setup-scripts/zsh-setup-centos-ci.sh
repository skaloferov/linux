#--------------------------------------------------------#
#                     Spas Kaloferov                     #
#                   www.kaloferov.com                    #
# bit.ly/The-Twitter      Social     bit.ly/The-LinkedIn #
# bit.ly/The-Gitlab        Git        bit.ly/The-Youtube #
# bit.ly/The-BSD         License          bit.ly/The-GNU #
#--------------------------------------------------------#

  #
  #              Shell Script Code Sample     
  #
  #  - For CentOS cloud images
  #  - Installs zsh & oh-my-zsh 
  #  - Enables zsh with sudo
  #  - Applyes theme from git to cloud user and root. 
  #

# ------------- Install Zsh and Oh-my-zsh -------------- #

sudo -s 
cd /tmp  

# Isntall zsh and oh-my-zsh
yum install zsh -y
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"  "" --unattended

# ---------------- Change Shell to Zsh ----------------- #

# Change Shell for Root user
chsh -s $(which zsh)
zsh

# Change Shell for CentOS user
chsh --shell /bin/zsh centos
#grep centos /etc/passwd

# ---------------- Enable Zsh with Sudo ---------------- #

# to enable on zsh with sudo set 
sed -i -e '$aSHELL=/bin/zsh' /etc/zshenv

# ------------------ Apply Zsh config ------------------ #

# add zsh for CentOS user
cp ~/.zshrc /home/centos/.zshrc
chown -R centos /home/centos/.zshrc
cp -a ~/.oh-my-zsh/ /home/centos/.oh-my-zsh/
chown -R centos /home/centos/.oh-my-zsh/
sed -i 's/\/root\/.oh-my-zsh/\/home\/centos\/.oh-my-zsh/g' /home/centos/.zshrc

# Download and change oh-my-zsh theme
curl -L 'https://gitlab.com/skaloferov/linux/-/raw/master/apps/oh-my-zsh/themes/spas.zsh-theme' -o spas.zsh-theme
cp spas.zsh-theme /root/.oh-my-zsh/themes/spas.zsh-theme
cp spas.zsh-theme /home/centos/.oh-my-zsh/themes/spas.zsh-theme
sed -i 's/ZSH_THEME="robbyrussell"/ZSH_THEME="spas"/g' /root/.zshrc
sed -i 's/ZSH_THEME="robbyrussell"/ZSH_THEME="spas"/g' /home/centos/.zshrc

# ---------------------- Appendix ---------------------- #

## install with 
#   curl -L 'https://gitlab.com/skaloferov/linux/-/raw/master/apps/oh-my-zsh/setup-scripts/zsh-setup-centos-ci.sh' -o zsh-setup-centos-ci.sh
#   chmod u+x *.sh
#   bash zsh-setup-centos-ci.sh

## Important ZSH Files
# /etc/zshenv    # Read for every shell
# ~/.zshenv      # Read for every shell except ones started with -f
# /etc/zprofile  # Global config for login shells, read before zshrc
# ~/.zprofile    # User config for login shells
# /etc/zshrc     # Global config for interactive shells
# ~/.zshrc       # User config for interactive shells
# /etc/zlogin    # Global config for login shells, read after zshrc
# ~/.zlogin      # User config for login shells
# ~/.zlogout     # User config for login shells, read upon logout
# /etc/zlogout   # Global config for login shells, read after user logout file


#--------------------------------------------------------#
#                     Spas Kaloferov                     #
#                   www.kaloferov.com                    #
# bit.ly/The-Twitter      Social     bit.ly/The-LinkedIn #
# bit.ly/The-Gitlab        Git        bit.ly/The-Youtube #
# bit.ly/The-BSD         License          bit.ly/The-GNU #
#--------------------------------------------------------#

  #
  #               Zsh Theme Code Sample     
  #
  #  - Zsh Theme 
  #

# ----------------------- Theme ------------------------ #

local c0=$(printf "\033[0m")
local c1=$(printf "\033[38;5;215m")
local c2=$(printf "\033[38;5;209m")
local c3=$(printf "\033[38;5;203m")
local c4=$(printf "\033[33;4m")
local c5=$(printf "\033[38;5;137m")
local c6=$(printf "\033[38;5;240m")
local c7=$(printf "\033[38;5;149m")
local c8=$(printf "\033[38;5;126m")
local c9=$(printf "\033[38;5;162m")
local c10=$(printf "\033[38;5;181m")

local cy1=$(printf "\033[38;5;11m")
local co1=$(printf "\033[38;5;173m")
local co2=$(printf "\033[38;5;180m")
local cg1=$(printf "\033[38;5;108m")
local cg2=$(printf "\033[38;5;144m")
local cg3=$(printf "\033[38;5;150m")
local cm1=$(printf "\033[38;5;137m")

if [ "$TERM" = "linux" ]; then
    c1=$(printf "\033[34;1m")
    c2=$(printf "\033[35m")
    c3=$(printf "\033[31m")
    c4=$(printf "\033[31;1m")
    c5=$(printf "\033[32m")
    c6=$(printf "\033[32;1m")
    c7=$(printf "\033[33m")
    c8=$(printf "\033[33;1m")
    c9=$(printf "\033[34m")
fi
local user="%{$c3%}%n%{$c6%} "
local host="@ %{$co2%}%m%{$c6%} "
local pwd="%{$cg3%}%/%{$c6%}%{$c6%} "
local priv="%(!.#.$)"

#local user="%{$c3%}%n%{$c6%} "
#local host="@ %{$c10%}%m%{$c6%} "
#local pwd="%{$c7%}%/%{$c6%}%{$c6%} "
#local priv="%(!.#.$)"

local git="%{$c6%}$(git_prompt_info)"
local res="%{$reset_color%} "
local date="%{$c6%}[%*] "
local date2="%{$c5%}[%*]"

PROMPT='${user}${host}${pwd}${priv}${res}'

#PROMPT='%{$c3%}%n%{$c6%} @ %{$c1%}%m%{$c6%} %{$c7%}%/%{$c6%}%{$c6%} %(!.#.$)%{$reset_color%} '
# PROMPT='%{$c3%}%n%{$c6%} @ %{$c1%}%M%{$c6%}:%{$c7%}%/%{$c6%}$(git_prompt_info) %{$c6%}%(!.#.$) %{$reset_color%}'
#RPROMPT='%{$c5%}[%*]%{$reset_color%}'
RPROMPT='$(ruby_prompt_info) %{$c6%}$(git_prompt_info)%{$reset_color%}'

ZSH_THEME_GIT_PROMPT_PREFIX="%{$c6%}[git:"
ZSH_THEME_GIT_PROMPT_SUFFIX="]%{$reset_color%}"

ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[red]%} ✗%{$c6%}"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[green]%} ✔%{$c6%}"

# Different ticks
#ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[red]%}✖%{$c6%}"
#ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[green]%}✔%{$c6%}"

#LS_COLORS='rs=0:di=01;40:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.axa=00;36:*.oga=00;36:*.spx=00;36:*.xspf=00;36:'

# ---------------------- Appendix ---------------------- #

# More references here http://zsh.sourceforge.net/Doc/Release/Prompt-Expansion.html
# and here https://www.2daygeek.com/understanding-the-color-code-of-linux-files/


